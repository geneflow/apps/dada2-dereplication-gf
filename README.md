DADA2 Dereplication GeneFlow App 
================================

Version: 0.1

This GeneFlow app wraps the DADA2 (R/Bioconductor) dereplication pipeline for processing amplicon sequencing data. 

See publication and github for DADA2 information.  
https://doi.org/10.1038/nmeth.3869
https://github.com/benjjneb/dada2

Briefly
1. The forward and reverse reads are quality filtered.
2. Forward and reverse reads are merged and unique sequence variants are counted.

Data Requirement
----------------
Paired reads should be generated from amplicon sequencing with significant overlap (>20bp) between the forward and reverse reads.

Inputs
------
1. files: Input directory containing paired-end reads.

Parameters
----------
1. output: Name of output directory.
2. length: length of reads.
3. trim: maximum length to trim from end of read. 2x for the reverse read.

Outputs Files
-------------
1. OTU.csv - Column names corresponding to input file. Row names corresponding to sequence variant number. Table contains the count of sequence variant for each input file forward and reserve pair. 
2. DADA2_OTU.csv - Same as OTU.csv except row names contain the exact sequence of the variant.
3. OTU.fasta - Fasta file with the sequence variant numbers as the header and the exact sequences of the variants.
4. QC.csv - Quality control information. Input column is the number of reads from the input fasta file. Filtered column is the number of reads passing filtering. Merged column is the number of reads that successfully merged. Greater than 50% loss from input to merged indicates problems with sequencing and/or experimental design. The most common problem is forward and reverse reads not overlapping because the amplicon is too long to cover with high quality reads using short reads. 
